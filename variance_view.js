VarianceView = function(){
	var width = 0.7*window.innerWidth;
	var height = 0.95 * window.innerHeight;
	var xAxisHeight = 60;
	var yAxisWidth = 60;
	
	var zoomHeight = 0.95*height + 1;
	var qualityHeight = 0.05*height - 4;
	
	var currentColor;
	var colors = [];
	var storedZoomData = []; //FIXME: names not ideal...
	var storedNonZoomData = [];
	var activeIndices = [];
	var zoomMins = [];
	var zoomMaxs = [];
	var nonZoomMins = [];
	var nonZoomMaxs = [];
	var dataSource = 'hacc';
	var currentNorm = 'none';
	var currentScale = 'log';
	
	this.showOperation = function(event, frac, color){
    	addOperation(frac, color)
    }
    
    this.updateSource = function(event, source){
    	updateSource(source);
    }

	var x_min = Infinity,
		x_max = -Infinity,
		y_min = Infinity,
		y_max = -Infinity;
		
	var zoom_y_min = Infinity,
		zoom_y_max = -Infinity;
		
	var zoomYAxis = d3.axisLeft();
	
	var sizes = ['0.001', '0.005', '0.01'];
	
	var zoomChart;

	var plotContainer = d3.select("body").append("svg")
	  .attr("width", width)
	  .attr("height", height);

	plotContainer.append("rect")
	  .attr("x", 0)
	  .attr("y", 0)
	  .attr("height", height)
	  .attr("width", width)
	  .attr("class", "border");
	  
	var zoomGroup = plotContainer.append("g")
	  .attr("x", 0)
	  .attr("y", 0)
	  .attr("height", zoomHeight);

	 var qualityGroup = plotContainer.append("g")
		.attr("x", 0)
		.attr("y", 0)
		.attr("transform", function(){ return "translate(0," + (height-50) + ")"; })
		.attr("width", width)
		.attr("height", qualityHeight);
	
	// main view ("chart"):
	function addVariance(filename){
		console.log("filename: ", filename);
		d3.csv(filename, function(error0, data0) {
			  console.log("raw data: ", data0);
			  var zoomData = new Array(data0.length);
			  var nonZoomData = new Array(data0.length);
			  var x_var = (dataSource == 'cmip' ? 'time' : 'mass');

			  pushData = function(d, i, idx, zoomArr, nonZoomArr){
			  		//console.log("data to parse: ", d);
			  		var x_val = parseFloat(d[x_var]),
			  		    x_bin = idx;
					variance = parseFloat(d.variance),
					min = parseFloat(d.min),
					max = parseFloat(d.max),
					med = parseFloat(d.median),
					upper = parseFloat(d.upper),
					lower = parseFloat(d.lower);
					zoomArr[idx] = [x_val, variance, min - med, max - med, 0, lower - med, upper - med, x_bin];
					nonZoomArr[idx] = [x_val, variance, min, max, med, lower, upper, x_bin];
		
					if (x_val > x_max) x_max = x_val;
					if (x_val < x_min) x_min = x_val;
		
					if (max > y_max) y_max = max;
					if (min < y_min) y_min = min;
		
					if (max - med > zoom_y_max) zoom_y_max = max - med;
					if (min - med < zoom_y_min) zoom_y_min = min - med;
			  };
			  var i = 0;
			  var idx = 0;
			  data0.forEach(function(x) {
			  	pushData(x, i, idx, zoomData, nonZoomData);
			  	i += 3;
			  	idx++;
			  });
			  
			  //FIXME: could be more elegant
			  storedZoomData.push(zoomData);
			  storedNonZoomData.push(nonZoomData);
			  zoomMins.push(zoom_y_min);
			  zoomMaxs.push(zoom_y_max);
			  nonZoomMins.push(y_min);
			  nonZoomMaxs.push(y_max);
			  
			  xmin = x_min;
			  xmax = x_max;
			  
			  updateView();
		});
	}
	function updateView(){
		  console.log("active indices: ", activeIndices);
		  var ymin, ymax;	  
		  var currentData = [];
		  if (currentNorm == 'none') { 
			currentData = storedNonZoomData[activeIndices]; 
			ymin = Math.min.apply(Math, zoomMins);
			ymax = Math.max.apply(Math, zoomMaxs);
		  }
		  else { 
			currentData = storedZoomData[activeIndices]; 
			ymin = Math.min.apply(Math, nonZoomMins);	
			ymax = Math.max.apply(Math, nonZoomMaxs);
		  }
		  console.log("current data: ", currentData);
						  
		  var boxWidth = 0.7*(width - yAxisWidth)/currentData.length;
		  var qualityData = Array.from({length:currentData.length}, () => Math.random());
					   
		  zoomChart = d3.box(width - yAxisWidth, 0.9*height-44, boxWidth)
			.whiskers(iqr());

		  
		  zoomChart.x_domain([x_min, x_max]);
		  zoomChart.y_domain([ymin, ymax]);
		  zoomChart.width([width/currentData.length]);

		  xscale = d3.scaleLog()
				  .domain([xmin, xmax])
				  .range([0, width - yAxisWidth]);
				  
		  xBinScale = d3.scaleLinear()
				  .domain([0,currentData.length]) //FIXME
				  .range([0, width - yAxisWidth]);
				  
		  if (currentScale == 'lin') {
			  zoomYScale = d3.scaleLinear()
					.domain([ymin, ymax])
					.nice()
					.range([0.9*height-4, 40])
					.nice();
			}
		else if (currentScale == 'log'){
				zoomYScale = d3.scaleLog()
					.domain([ymin, ymax])
					.nice()
					.range([0.9*height-4, 40])
					.nice();
		}
				
		var xAxis = d3.axisBottom()
			.scale(xscale)
			.tickSize(10)
			.ticks(2)
			.tickFormat(d3.format(".1e"));

		var binXAxis = d3.axisBottom()
			.scale(xBinScale)
			.tickSize(10)
			.ticks(10)
			.tickFormat(d3.format(".1e"));
	
		zoomYAxis = d3.axisLeft()
			.tickSizeOuter(0)
			.scale(zoomYScale)
			.tickSize(10)
			.ticks(5)
			.tickFormat(d3.format(".1e"));

		d3.select("box").selectAll("*").remove();
		d3.selectAll(".enter").remove();

		var zoomBars = zoomGroup.selectAll("box")
			.data(currentData);
			
		zoomBars.attr("class", "update");
			
		zoomBars.enter()
			.append("svg")
			.attr("class", "enter box")
			.attr("x", function(d, i) { return yAxisWidth + xBinScale(d[7]);}) //FIXME
			.attr("y", 44)
			.attr("fill", function(d,i) { return currentColor; })
			.attr("stroke", function() { return currentColor; })
			.attr("width", width)
			.attr("height", 0.9*height-44)
			.call(zoomChart);
			  
		 zoomBars.exit().remove();
	 
		 zoomGroup.append("g")
			  .attr("transform", function(){ return "translate(" + 52 + "," + (zoomHeight-34) + ")"; })
			  .call(binXAxis);

		  zoomGroup.append("svg")
			  .attr("width", 60)
			  .attr("height", zoomHeight)
			  .attr("y", 0)
			.append("g")
			  .attr("width", 60)
			  .attr("transform", "translate(" + 50 + "," + 4 + ")")
			  .call(zoomYAxis);
			
		  qualityGroup.selectAll("g")
			.data(qualityData)
			.enter()
			.append("rect")
			.attr("x", function(d,i){ return (boxWidth+5.7)*i;})
			.attr("y", 20)
			.attr("transform", "translate(60,0)")
			.attr("width", boxWidth)
			.attr("height", boxWidth)
			.attr("fill", function(d){ return d3.interpolateGreys(d + 0.2);}) //FIXME: fill with real data
			.on("mouseover", showConvergence)
			.on("mouseout", hideConvergence); //FIXME: also fill with real data
	}
	function iqr() {
	  return function(d, i) {
		var q1 = d[5],
			q3 = d[6];
		return [q1, q3];
	  };
	}

   var update = function() {
	   var lines = d3.select("body").select("path").attr("d", this.line(this.points)); //FIXME: for when we have grid axes
	}
		
	function addOperation(frac, color){
		zoomChart = null;
		delete zoomChart;
		var fn;
		if (dataSource == 'cmip'){ //FIXME
			fn = 'data/' + dataSource + '/CMIP_variance_N_200_frac_' + frac + '_adjusted.csv';
		}
		else {
			var suffix = '_0.1_130-150.csv';
			fn = 'data/' + dataSource + '/Variance_Average_' + frac + suffix;
		}
		currentColor = color;
		colors.push(currentColor);
		plotContainer.selectAll(".checkbox")
			.data(colors)
			.enter()
			.append("rect")
			.attr("class", "checkbox")
			.attr("x", function(d,i){ return 6 + i * 24; })
			.attr("y", 6)
			.attr("width", 20)
			.attr("height", 20)
			.attr('stroke-width', 2)
			.attr("stroke", function(d, i){ return d; })
			.attr("fill", function(d,i){ return d; })
			.attr("fill-opacity", 0.0)
			.on("click", toggleCheckbox);
		
		console.log("loading variance from file " + fn);
		addVariance(fn);
		
	}
	//FIXME
	const varChartSize = 80;
	
	var xConvScale = d3.scaleLinear()
		.domain([1,20])
		.range([0,varChartSize])
	var yConvScale = d3.scaleLinear()
		.domain([0,1])
		.range([0,varChartSize]);
	
	var lineFunction = d3.line()
	  .x(function(d) { return xConvScale(d.x); })
	  .y(function(d) { return yConvScale(1-d.y); });
	  //.curve('linear');
	function showConvergence(d,i){
			
		//FIXME: PLACEHOLDER
		var convergenceData = [ //this is fake
		{'y': 0.2, 'x': 1},
		{'y': 0.4, 'x': 2},
		{'y': 0.3, 'x': 3},
		{'y': 0.5, 'x': 4},
		{'y': 0.4, 'x': 5},
		{'y': 0.45, 'x': 6},
		{'y': 0.34, 'x': 7},
		{'y': 0.6, 'x': 8},
		{'y': 0.24, 'x': 9},
		{'y': 0.55, 'x': 10},
		{'y': 0.7, 'x': 11},
		{'y': 0.51, 'x': 12},
		{'y': 0.43, 'x': 13},
		{'y': 0.53, 'x': 14},
		{'y': 0.5, 'x': 15},
		{'y': 0.6, 'x': 16},
		{'y': 0.54, 'x': 17},
		{'y': 0.53, 'x': 18},
		{'y': 0.52, 'x': 19},
		{'y': 0.53, 'x': 20}];
		d3.selectAll(".varChart").remove();
		var x = parseFloat(d3.select(this).attr("x"));
		if (x > width - yAxisWidth - varChartSize) x -= varChartSize;
		console.log("x: ", x);
		var chartBox = d3.select(this.parentNode).append("rect")
			.attr("class", "varChart")
			.attr("transform", function(){ return "translate(" + (x + yAxisWidth) + "," + -(varChartSize - 12) + ")"; })
			.attr("width", varChartSize)
			.attr("height", varChartSize)
			.attr("stroke", "#707070")
			.attr("fill", "white")
			.attr("stroke-width", "2px");
		var chart = d3.select(this.parentNode).append("path")
			.data([convergenceData])
			.attr("transform", function(){ return "translate(" + (x + yAxisWidth) + "," + -(varChartSize - 12) + ")"; })
			.attr("class", "line varChart")
			.attr("d", lineFunction)
			.attr('stroke', '#707070')
			.attr('stroke-width', '2px')
			.attr('fill', 'none');
	}
	function hideConvergence(d,i){
		d3.selectAll(".varChart").remove();
	}
	
	function updateSource(source){
		console.log("updating source in variance view: ", source);
		dataSource = source;
		d3.selectAll('.enter').remove();
	}
	
	function toggleCheckbox(d,i){
		if (d3.select(this).style("fill-opacity") == 0.0){
			d3.select(this).style("fill-opacity", 0.6);
			activeIndices.push(i);
		}
		else {
			d3.select(this).style("fill-opacity", 0.0);
			var index = activeIndices.indexOf(i);
			if (index > -1) {
				activeIndices.splice(index, 1);
			}
		}
		updateView();
	}
}
